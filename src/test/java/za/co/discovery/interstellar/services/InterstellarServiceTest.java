package za.co.discovery.interstellar.services;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import za.co.discovery.interstellar.InterstellarApplication;
import za.co.discovery.interstellar.domain.Planet;
import za.co.discovery.interstellar.domain.Route;
import za.co.discovery.interstellar.repository.PlanetRepository;
import za.co.discovery.interstellar.repository.RoutesRepository;
import static org.junit.Assert.*;



@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = InterstellarApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class InterstellarServiceTest {

    @Autowired
    InterstellarService interstellarService;

    @Autowired
    PlanetRepository planetRepository;

    @Autowired
    RoutesRepository routesRepository;

    @Before
    public void prepare() {
        Planet planetA = new Planet();
        planetA.setPlanetName("Earth");
        planetA.setPlanetNode("A");
        planetRepository.save(planetA);

        Planet planetB = new Planet();
        planetB.setPlanetNode("B");
        planetB.setPlanetName("Jupitor");
        planetRepository.save(planetB);

        Planet planetC = new Planet();
        planetC.setPlanetNode("C");
        planetC.setPlanetName("Mars");
        planetRepository.save(planetC);

        Route route1 = new Route();
        route1.setRouteId(1);
        route1.setPlanetOrigin("A");
        route1.setPlanetDestination("B");
        route1.setDistance(2.0);
        routesRepository.save(route1);

        Route route2 = new Route();
        route2.setRouteId(2);
        route2.setPlanetOrigin("A");
        route2.setPlanetDestination("C");
        route2.setDistance(5.0);
        routesRepository.save(route2);

        Route route3 = new Route();
        route3.setRouteId(3);
        route3.setPlanetOrigin("B");
        route3.setPlanetDestination("C");
        route3.setDistance(2.0);
        routesRepository.save(route3);
    }

    @Test
    public void testShortestPath() {
        assertEquals(4.0, interstellarService.shortestPaths("C").getMinLightYearDistance(), 0.0);
        assertEquals(0.0, interstellarService.shortestPaths("A").getMinLightYearDistance(), 0.0);
        assertEquals(2.0, interstellarService.shortestPaths("B").getMinLightYearDistance(), 0.0);
    }
}