package za.co.discovery.interstellar.algorithm;

import org.springframework.stereotype.Component;
import za.co.discovery.interstellar.domain.Route;
import za.co.discovery.interstellar.dto.Edge;
import za.co.discovery.interstellar.dto.Vertex;

import java.util.HashMap;
import java.util.PriorityQueue;

@Component
public class DijkstraAlgorithm {

    public void calculateShortestPath(HashMap<String, Vertex> vertexMap, String sourcePlanet, Iterable<Route> routes) {
        Vertex sourceVertex = vertexMap.get(sourcePlanet);

        for (Route inputRoutes : routes) {
            Vertex vertex = vertexMap.get(inputRoutes.getPlanetOrigin());
            vertex.addNeighbour(new Edge(inputRoutes.getDistance(), vertexMap.get(inputRoutes.getPlanetOrigin()), vertexMap.get(inputRoutes.getPlanetDestination())));
        }

        sourceVertex.setDistance(0);
        PriorityQueue<Vertex> priorityQueue = new PriorityQueue<>();
        priorityQueue.add(sourceVertex);
        sourceVertex.setVisited(true);

        while (!priorityQueue.isEmpty()) {
            Vertex actualVertex = priorityQueue.poll();

            for (Edge edge : actualVertex.getAdjacenciesList()) {

                Vertex v = edge.getTargetVertex();
                if (!v.isVisited()) {
                    double newDistance = actualVertex.getDistance() + edge.getWeight();

                    if (newDistance < v.getDistance()) {
                        priorityQueue.remove(v);
                        v.setDistance(newDistance);
                        v.setPredecessor(actualVertex);
                        priorityQueue.add(v);
                    }
                }
            }
            actualVertex.setVisited(true);
        }
    }
}
