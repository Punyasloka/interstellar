package za.co.discovery.interstellar.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import za.co.discovery.interstellar.domain.Planet;
import za.co.discovery.interstellar.domain.Route;
import za.co.discovery.interstellar.services.InterstellarService;

/**
 * This controller will allow you to do CRUD operation on input dto
 */
@RestController
@RequestMapping("api/planet")
public class InterstellarController {


    @Autowired
    InterstellarService interstellarService;

    /**
     * Get all planets.
     */
    @RequestMapping(method = RequestMethod.GET)
    public Iterable<Planet> getPlanets() {
        return interstellarService.getPlanetNames();
    }

    /**
     * Add new planet.
     */
    @RequestMapping(value="/addPlanet", method = RequestMethod.POST)
    public Planet addPlanet(@RequestBody Planet planet) {
        return interstellarService.addPlanet(planet);
    }

    /**
     * update a existing planet.
     */
    @RequestMapping(value = "/updatePlanet", method = RequestMethod.PUT)
    public Planet updatePlanet(@RequestBody Planet planet) {
        return interstellarService.updatePlanet(planet);
    }

    /**
     * delete a planet based on planet name.
     */
    @RequestMapping(value = "/deletePlanet/{planetName}", method = RequestMethod.DELETE)
    public ResponseEntity deletePlanet(@PathVariable String planetName) {
        interstellarService.deletePlanet(planetName);
        return new ResponseEntity("Planet deleted successfully", HttpStatus.OK);
    }


    /**
     * Get all the routes.
     */
    @RequestMapping(value = "/routes",method = RequestMethod.GET)
    public Iterable<Route> getRoutes() {
        return interstellarService.getRoutes();
    }

    /**
     * Add new route.
     */
    @RequestMapping(value = "/addRoute", method = RequestMethod.POST)
    public Route addRoute(@RequestBody Route route) {
        return interstellarService.addRoute(route);
    }

    /**
     * Update an existing route.
     */
    @RequestMapping(value = "/updateRoute", method = RequestMethod.PUT)
    public Route updateRoute(@RequestBody Route route) {
        return interstellarService.updateRoute(route);
    }

    /**
     * Delete a route based on route id.
     */
    @RequestMapping(value = "/deleteRoute/{routeId}", method = RequestMethod.DELETE)
    public ResponseEntity deleteRoute(@PathVariable int routeId) {
        interstellarService.deleteRoute(routeId);
        return new ResponseEntity("Route deleted successfully", HttpStatus.OK);
    }

}
