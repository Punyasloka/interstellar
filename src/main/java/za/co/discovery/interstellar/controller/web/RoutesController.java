package za.co.discovery.interstellar.controller.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import za.co.discovery.interstellar.domain.Planet;
import za.co.discovery.interstellar.domain.Route;
import za.co.discovery.interstellar.services.InterstellarService;

/**
 * This controller is for the UI to add and manage routes.
 */

@RestController
@RequestMapping("routes")
public class RoutesController {

    @Autowired
    InterstellarService interstellarService;

    /**
     * To view all the routes.
     */
    @GetMapping("/showRoutes")
    public ModelAndView showAllRoutes() {
        ModelAndView modelAndView = new ModelAndView("all-routes");
        modelAndView.addObject("routes", interstellarService.getRoutes());
        return modelAndView;
    }

    /**
     * Create your own route
     */
    @GetMapping("/create")
    public ModelAndView homePage() {
        ModelAndView modelAndView = new ModelAndView("create-routes");
        modelAndView.addObject("routes", new Route());
        return modelAndView;
    }

    /**
     * persist all the routes to database
     */
    @PostMapping("/save")
    public ModelAndView saveRoutes(@ModelAttribute Route routes, Model model) {
        ModelAndView modelAndView = new ModelAndView("redirect:/routes/showRoutes");
        if(interstellarService.checkIfPlanetNodeExist(routes.getPlanetOrigin()) && interstellarService.checkIfPlanetNodeExist(routes.getPlanetDestination())) {
            interstellarService.addRoute(routes);
            model.addAttribute("routes", interstellarService.getRoutes());
        } else {
            System.out.println("Route doesn't exist");
        }
        return modelAndView;
    }

    /**
     * delete the existing routes.
     */
    @GetMapping("/delete")
    public ModelAndView deleteRoutes() {
        ModelAndView modelAndView = new ModelAndView("redirect:/routes/showRoutes");
        interstellarService.deleteAllRoutes();
        return modelAndView;
    }

    /**
     * This ll allow you to choose your source plaent
     */
    @GetMapping("/path")
    public ModelAndView calculateShortestPath() {
        ModelAndView modelAndView = new ModelAndView("routes-path");
        modelAndView.addObject("planet", new Planet());
        return modelAndView;
    }

    /**
     * To view shortest distance to all the planets from source planet.
     */
    @PostMapping("/shortestPath")
    public ModelAndView calculateShortestPathForSource(@ModelAttribute Planet planet, Model model) {
        ModelAndView modelAndView = new ModelAndView("shortest-path");
        modelAndView.addObject("routeDetails", interstellarService.shortestPathToAllDestinations(planet.getPlanetNode()));
        return modelAndView;
    }

}
