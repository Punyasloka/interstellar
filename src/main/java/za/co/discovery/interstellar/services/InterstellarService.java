package za.co.discovery.interstellar.services;

import interstellar.za.co.discovery.routes.RouteDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import za.co.discovery.interstellar.algorithm.DijkstraAlgorithm;
import za.co.discovery.interstellar.dto.Vertex;
import za.co.discovery.interstellar.domain.Planet;
import za.co.discovery.interstellar.domain.Route;
import za.co.discovery.interstellar.repository.PlanetRepository;
import za.co.discovery.interstellar.repository.RoutesRepository;
import za.co.discovery.interstellar.repository.TrafficRepository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.PriorityQueue;


@Service
public class InterstellarService {

    @Autowired
    PlanetRepository planetRepository;

    @Autowired
    RoutesRepository routesRepository;

    @Autowired
    TrafficRepository trafficRepository;

    @Autowired
    DijkstraAlgorithm dijkstraAlgorithm;

    private final String SOURCE_PLANET = "A";

    public Iterable<Planet> getPlanetNames() {
        return planetRepository.findAll();
    }

    public Planet addPlanet(Planet planet) {
        return planetRepository.save(planet);
    }

    public void deletePlanet(String planetName) {
        Planet planet = planetRepository.findByPlanetName(planetName);
        planetRepository.delete(planet);
    }

    public Planet getPlanetByNode(String planetNode) {
        return planetRepository.findByPlanetNode(planetNode);
    }

    public Planet updatePlanet(Planet planet) {
        Planet planetData = planetRepository.findByPlanetNode(planet.getPlanetNode());
        planetData.setPlanetName(planet.getPlanetName());
        return planetRepository.save(planetData);
    }

    public Iterable<Route> getRoutes() {
        return routesRepository.findAll();
    }

    public Route addRoute(Route route) {
        return routesRepository.save(route);
    }

    public Route updateRoute(Route route) {
        Route routeData = routesRepository.findByRouteId(route.getRouteId());
        routeData.setPlanetDestination(route.getPlanetDestination());
        routeData.setPlanetOrigin(route.getPlanetOrigin());
        routeData.setDistance(route.getDistance());
        return routesRepository.save(routeData);
    }

    public void deleteRoute(int routeId) {
        Route route = routesRepository.findByRouteId(routeId);
        routesRepository.delete(route);
    }

    public void deleteAllRoutes() {
        routesRepository.deleteAll();
    }


    /**
     * Calculates the shortest path from earth to any destination planet given by user
     */
    public RouteDetails shortestPaths(String destinationPlanetName) {
        Iterable<Route> routes = getRoutes();
        Iterable<Planet> planets = getPlanetNames();

        HashMap<String, Vertex> vertexMap = new HashMap();
        for (Planet planet : planets) {
            vertexMap.put(planet.getPlanetNode(), new Vertex(planet.getPlanetNode()));
        }

        dijkstraAlgorithm.calculateShortestPath(vertexMap, SOURCE_PLANET, routes);

        RouteDetails routeDetails = new RouteDetails();
        routeDetails.setSourcePlanetName(SOURCE_PLANET);
        routeDetails.setDestinationPlanetName(destinationPlanetName);
        routeDetails.setMinLightYearDistance(vertexMap.get(destinationPlanetName).getDistance());
        return routeDetails;

    }

    public boolean checkIfPlanetNodeExist(String planetNode) {
        if (this.getPlanetByNode(planetNode) != null) {
            return true;
        }
        return false;
    }


    /**
     * Calculate the shortest path to all destination planets from source.
     *
     * It also allows you to change the source planet and recalculate the shortest path
     */
    public List<RouteDetails> shortestPathToAllDestinations(String sourcePlanet) {
        Iterable<Route> routes = getRoutes();
        Iterable<Planet> planets = getPlanetNames();

        HashMap<String, Vertex> vertexMap = new HashMap();
        for (Planet planet : planets) {
            vertexMap.put(planet.getPlanetNode(), new Vertex(planet.getPlanetNode()));
        }


        dijkstraAlgorithm.calculateShortestPath(vertexMap, sourcePlanet, routes);
        List<RouteDetails> routeDetailsList = new ArrayList<>();
        for (Planet planet : planets) {
            RouteDetails routeDetails = new RouteDetails();
            routeDetails.setSourcePlanetName(sourcePlanet);
            routeDetails.setDestinationPlanetName(planet.getPlanetNode());
            routeDetails.setMinLightYearDistance(vertexMap.get(planet.getPlanetNode()).getDistance());
            routeDetailsList.add(routeDetails);
        }
        return routeDetailsList;

    }


}
