package za.co.discovery.interstellar.services;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;
import za.co.discovery.interstellar.domain.Planet;
import za.co.discovery.interstellar.domain.Route;
import za.co.discovery.interstellar.repository.PlanetRepository;
import za.co.discovery.interstellar.repository.RoutesRepository;
import za.co.discovery.interstellar.repository.TrafficRepository;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;

/**
 * This component is responsible for loading the input file and persist to database
 */
@Component
public class FileReaderService {

    private static Logger log = LoggerFactory.getLogger(FileReaderService.class);
    @Autowired
    PlanetRepository planetRepository;

    @Autowired
    RoutesRepository routesRepository;

    @Autowired
    TrafficRepository trafficRepository;


    /**
     * loading dto from the sheets of input file(.xls) and persisting it to database while starting up the application.
     */
    @PostConstruct
    public void loadData() throws IOException, InvalidFormatException {
//        Workbook workbook = WorkbookFactory.create(new File(ClassLoader.getSystemResource("data.xlsx").getFile()));
        Workbook workbook = WorkbookFactory.create(new ClassPathResource("data.xlsx").getInputStream());
        log.info("Workbook has :" + workbook.getNumberOfSheets());
        DataFormatter dataFormatter = new DataFormatter();
        workbook.forEach(sheet -> {
            log.info("sheet name is :" + sheet.getSheetName());
            if(sheet.getSheetName().equalsIgnoreCase("Planet Names")) {
                sheet.forEach(row -> {
                    if(row.getRowNum() != 0) {
                        Planet planetNames = new Planet();
                        row.forEach(cell -> {
                            String cellValue = dataFormatter.formatCellValue(cell);
                            if(planetNames.getPlanetNode() == null) {
                                planetNames.setPlanetNode(cellValue);
                            } else if(planetNames.getPlanetName() == null) {
                                planetNames.setPlanetName(cellValue);
                            }
                            planetRepository.save(planetNames);
                        });
                    }
                });

            } else if(sheet.getSheetName().equalsIgnoreCase("Routes")) {
                sheet.forEach((row -> {
                    if(row.getRowNum() != 0) {
                        Route routes = new Route();
                        row.forEach(cell -> {
                            String cellValue = dataFormatter.formatCellValue(cell);
                            if(routes.getRouteId() == 0) {
                                routes.setRouteId(Integer.parseInt(cellValue));
                            } else if(routes.getPlanetOrigin() == null) {
                                routes.setPlanetOrigin(cellValue);
                            } else if(routes.getPlanetDestination() == null) {
                                routes.setPlanetDestination(cellValue);
                            } else if( routes.getDistance() == null) {
                                routes.setDistance(Double.parseDouble(cellValue));
                            }
                            routesRepository.save(routes);
                        });
                    }
                }));
            }
        });
        workbook.close();
    }
}
