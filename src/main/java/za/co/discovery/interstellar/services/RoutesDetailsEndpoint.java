package za.co.discovery.interstellar.services;

import interstellar.za.co.discovery.routes.GetRoutesDetailsResponse;
import interstellar.za.co.discovery.routes.GetRoutesRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;


@Endpoint
public class RoutesDetailsEndpoint {

    @Autowired
    InterstellarService interstellarService;

    @PayloadRoot(namespace = "http://discovery.co.za.interstellar/routes", localPart = "GetRoutesRequest")
    @ResponsePayload
    public GetRoutesDetailsResponse processRouteDetailssRequest(@RequestPayload GetRoutesRequest request) {
        GetRoutesDetailsResponse response = new GetRoutesDetailsResponse();
        String sourcePlanetName = request.getDestinationPlanetName();
        response.setRoutesDetails(interstellarService.shortestPaths(sourcePlanetName));
        return response;
    }
}
