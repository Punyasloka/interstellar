package za.co.discovery.interstellar.domain;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "traffic")
public class Traffic {

    @Id
    private String routeId;
    private String planetOrigin;
    private String planetDestination;
    private String trafficDelay;

    public String getRouteId() {
        return routeId;
    }

    public void setRouteId(String routeId) {
        this.routeId = routeId;
    }

    public String getPlanetOrigin() {
        return planetOrigin;
    }

    public void setPlanetOrigin(String planetOrigin) {
        this.planetOrigin = planetOrigin;
    }

    public String getPlanetDestination() {
        return planetDestination;
    }

    public void setPlanetDestination(String planetDestination) {
        this.planetDestination = planetDestination;
    }

    public String getTrafficDelay() {
        return trafficDelay;
    }

    public void setTrafficDelay(String trafficDelay) {
        this.trafficDelay = trafficDelay;
    }
}
