package za.co.discovery.interstellar.repository;

import org.springframework.data.repository.CrudRepository;
import za.co.discovery.interstellar.domain.Traffic;

public interface TrafficRepository extends CrudRepository<Traffic, Long> {
}
