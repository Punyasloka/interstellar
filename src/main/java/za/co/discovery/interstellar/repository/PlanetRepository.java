package za.co.discovery.interstellar.repository;

import org.springframework.data.repository.CrudRepository;
import za.co.discovery.interstellar.domain.Planet;

public interface PlanetRepository extends CrudRepository<Planet, Long> {

    Planet findByPlanetName(String planetName);

    Planet findByPlanetNode(String planetNode);
}
