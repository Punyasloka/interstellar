package za.co.discovery.interstellar.repository;

import org.springframework.data.repository.CrudRepository;
import za.co.discovery.interstellar.domain.Route;

public interface RoutesRepository extends CrudRepository<Route, Long> {

    Route findByRouteId(int routeId);
}
