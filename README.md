Local testing
-------------

mvn spring-boot:run

linenode server deployment url
------------------------------
http://104.237.156.100:8080/swagger-ui.html

Swagger UI
----------

http://localhost:8080/swagger-ui.html


WSDL for shortest path from Earth
---------------------------------
http://localhost:8080/ws/routes.wsdl


UI for add and manage routes
----------------------------
http://localhost:8080/routes/showRoutes
http://localhost:8080/routes/create
http://localhost:8080/routes/path
http://localhost:8080/routes/shortestPath


Technology
----------
Spring boot
in-memory Derby db
Apache poi
Swagger
Thymeleaf etc.


